from django.contrib import admin

from gilbertosHotelApp.models.habitacion import Habitacion
from .models.user import User
from .models.factura import Factura
from .models.habitacion import Habitacion
from .models.reserva import Reserva
from .models.servicio import Servicio
admin.site.register(User)
admin.site.register(Factura)
admin.site.register(Habitacion)
admin.site.register(Reserva)
admin.site.register(Servicio)
# Register your models here.
