from django.conf import settings
from rest_framework import status, views, generics
from rest_framework.response import Response

from rest_framework_simplejwt.backends import TokenBackend
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer

from gilbertosHotelApp.models.servicio import Servicio
from gilbertosHotelApp.serializers.servicioSerializer import ServicioSerializer


class ServicioCreateView(views.APIView):
    def post(self, request, *args, **kwargs):
        serializer = ServicioSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response( status=status.HTTP_201_CREATED)


class ServicioDetailView(generics.RetrieveAPIView):
    serializer_class = ServicioSerializer
    queryset = Servicio.objects.all()
    

    def get_queryset(self, *args, **kwargs):
        queryset = Servicio.objects.filter(id=self.kwargs['pk'])
        return queryset


class ServicioListView(generics.ListCreateAPIView):
    queryset = Servicio.objects.all()
    serializer_class = ServicioSerializer
    

    def list(self, request):
        queryset = self.get_queryset()
        serializer = ServicioSerializer(queryset, many=True)
        return Response(serializer.data)


class ServicioUpdateView(generics.UpdateAPIView):
    serializer_class = ServicioSerializer
    queryset = Servicio.objects.all()

    def put(self, request, *args, **kwargs):
        queryset = Servicio.objects.filter(id=self.kwargs['pk'])
        return super().update(request, *args, **kwargs)


class ServicioDeleteView(generics.DestroyAPIView):
    serializer_class = ServicioSerializer
    queryset = Servicio.objects.all()

    def delete(self, request, *args, **kwargs):

        return super().destroy(request, *args, **kwargs)
