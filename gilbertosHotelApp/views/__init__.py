from .userViews import UserCreateView, UserDetailView,UserListView,UserUpdateView,UserDeleteView
from .habitacionViews import HabitacionCreateView,HabitacionDetailView,HabitacionListView,HabitacionUpdateView,HabitacionDeleteView
from .servicioViews import ServicioCreateView,ServicioDetailView,ServicioListView,ServicioUpdateView,ServicioDeleteView
from .reservaViews import ReservaCreateView,ReservaDetailView,ReservaListView,ReservaUpdateView,ReservaDeleteView
from .facturaViews import FacturaCreateView,FacturaDetailView,FacturaListView,FacturaUpdateView,FacturaDeleteView