from django.conf import settings
from rest_framework import status, views, generics
from rest_framework.response import Response

from rest_framework_simplejwt.backends import TokenBackend
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer

from gilbertosHotelApp.models.habitacion import Habitacion
from gilbertosHotelApp.serializers.habitacionSerializer import HabitacionSerializer


class HabitacionCreateView(views.APIView):
    def post(self, request, *args, **kwargs):
        serializer = HabitacionSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response( status=status.HTTP_201_CREATED)


class HabitacionDetailView(generics.RetrieveAPIView):
    serializer_class = HabitacionSerializer
    queryset = Habitacion.objects.all()
    

    def get_queryset(self, *args, **kwargs):
        queryset = Habitacion.objects.filter(id=self.kwargs['pk'])
        return queryset


class HabitacionListView(generics.ListCreateAPIView):
    queryset = Habitacion.objects.all()
    serializer_class = HabitacionSerializer
    

    def list(self, request):
        queryset = self.get_queryset()
        serializer = HabitacionSerializer(queryset, many=True)
        return Response(serializer.data)


class HabitacionUpdateView(generics.UpdateAPIView):
    serializer_class = HabitacionSerializer
    queryset = Habitacion.objects.all()

    def put(self, request, *args, **kwargs):
        queryset = Habitacion.objects.filter(id=self.kwargs['pk'])
        return super().update(request, *args, **kwargs)


class HabitacionDeleteView(generics.DestroyAPIView):
    serializer_class = HabitacionSerializer
    queryset = Habitacion.objects.all()

    def delete(self, request, *args, **kwargs):

        return super().destroy(request, *args, **kwargs)
