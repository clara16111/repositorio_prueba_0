from django.conf import settings
from rest_framework import status, views, generics
from rest_framework.response import Response

from rest_framework_simplejwt.backends import TokenBackend
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer

from gilbertosHotelApp.models.factura import Factura
from gilbertosHotelApp.serializers.facturaSerializer import FacturaSerializer


class FacturaCreateView(views.APIView):
    def post(self, request, *args, **kwargs):
        serializer = FacturaSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response( status=status.HTTP_201_CREATED)


class FacturaDetailView(generics.RetrieveAPIView):
    serializer_class = FacturaSerializer
    queryset = Factura.objects.all()
    

    def get_queryset(self, *args, **kwargs):
        queryset = Factura.objects.filter(id=self.kwargs['pk'])
        return queryset


class FacturaListView(generics.ListCreateAPIView):
    queryset = Factura.objects.all()
    serializer_class = FacturaSerializer
    

    def list(self, request):
        queryset = self.get_queryset()
        serializer = FacturaSerializer(queryset, many=True)
        return Response(serializer.data)


class FacturaUpdateView(generics.UpdateAPIView):
    serializer_class = FacturaSerializer
    queryset = Factura.objects.all()

    def put(self, request, *args, **kwargs):
        queryset = Factura.objects.filter(id=self.kwargs['pk'])
        return super().update(request, *args, **kwargs)


class FacturaDeleteView(generics.DestroyAPIView):
    serializer_class = FacturaSerializer
    queryset = Factura.objects.all()

    def delete(self, request, *args, **kwargs):

        return super().destroy(request, *args, **kwargs)
