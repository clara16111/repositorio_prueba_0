from gilbertosHotelApp.models.reserva import Reserva
from rest_framework import serializers
class ReservaSerializer(serializers.ModelSerializer):
 class Meta:
  model = Reserva
  fields = ['fechaEntrada','fechaSalida','cantidadPersonas','user','habitacion']
  