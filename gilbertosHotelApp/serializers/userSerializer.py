from rest_framework import serializers
from gilbertosHotelApp.models.user import User

class UserSerializer(serializers.ModelSerializer):
 class Meta:
  model = User
  fields = ['id', 'username', 'name', 'email','documentType','document','phone']



def to_representation(self, obj):
   user = User.objects.get(id=obj.id)
   return {
      'id': user.id,
      'username': user.username,
      'name': user.name,
      'email': user.email,
        }