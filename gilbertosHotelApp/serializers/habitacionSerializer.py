from rest_framework import serializers
from gilbertosHotelApp.models.habitacion import Habitacion
class HabitacionSerializer(serializers.ModelSerializer):
 class Meta:
  model = Habitacion
  fields = ['descripcion', 'numeroCamas', 'precio','tipoHabitacion']