from gilbertosHotelApp.models.servicio import Servicio
from rest_framework import serializers
class ServicioSerializer(serializers.ModelSerializer):
 class Meta:
  model = Servicio
  fields = ['tipoServicio','precio']