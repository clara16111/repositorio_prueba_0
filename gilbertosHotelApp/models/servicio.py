from django.db import models
class Servicio(models.Model):
 id = models.AutoField(primary_key=True)
 tipoServicio = models.CharField('tipoServicio', max_length = 50, unique=True)
 precio=models.DecimalField(max_digits=15,decimal_places=2)