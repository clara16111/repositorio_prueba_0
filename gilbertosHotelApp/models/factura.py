from django.db import models
from .reserva import Reserva
from .servicio import Servicio
class Factura(models.Model):
 id = models.AutoField(primary_key=True)
 fecha=models.DateField()
 mediodepago=models.CharField(max_length=50)
 servicio = models.ForeignKey(Servicio,related_name='servicio',on_delete=models.CASCADE)
 reserva = models.ForeignKey(Reserva,related_name='reserva',on_delete=models.CASCADE)
 valor = models.DecimalField(max_digits=15,decimal_places=2)