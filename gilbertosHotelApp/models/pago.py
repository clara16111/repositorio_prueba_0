from django.db import models
from .user import User
class Pago(models.Model):
 id = models.AutoField(primary_key=True)
 medio=models.CharField(max_length=45)
 targeta=models.CharField(max_length=16)
 nombre=models.CharField(max_length=45)
 csv=models.CharField(max_length=3)
 cuotas=models.CharField(max_length=3, default='0')
 vencimiento=models.DateField
 usuario=models.ForeignKey(User, related_name='user', on_delete=models.CASCADE)


