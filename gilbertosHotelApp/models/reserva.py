from django.db import models
from .user import User
from .habitacion import Habitacion
class Reserva(models.Model):
 id = models.AutoField(primary_key=True)
 fechaEntrada = models.DateField()
 fechaSalida=models.DateField()
 cantidadPersonas=models.IntegerField(default=0)
 user=models.ForeignKey(User,related_name='user',on_delete=models.CASCADE)
 habitacion=models.ForeignKey(Habitacion,related_name='habitacion',on_delete=models.CASCADE)
