from django.db import models
class Habitacion(models.Model):
 id = models.AutoField(primary_key=True)
 descripcion = models.CharField('descripcion', max_length = 50)
 numeroCamas=models.IntegerField('NumeroCamas',default=0)
 precio=models.DecimalField(max_digits=20,decimal_places=2)
 tipoHabitacion= models.CharField('tipoHabitacion',max_length=100)
 