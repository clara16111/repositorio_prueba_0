from django.apps import AppConfig


class GilbertoshotelappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'gilbertosHotelApp'
