from django.urls import path
from rest_framework_simplejwt.views import (
    TokenObtainPairView, TokenRefreshView)
from gilbertosHotelApp import views

urlpatterns = [
    #User
    path('login/', TokenObtainPairView.as_view()),
    path('refresh/', TokenRefreshView.as_view()),
    path('user/', views.UserCreateView.as_view()),
    path('userlist/', views.UserListView.as_view()),
    path('user/<int:pk>/', views.UserDetailView.as_view()),
    path('user/update/<int:pk>/', views.UserUpdateView.as_view()),
    path('user/delete/<int:pk>/', views.UserDeleteView.as_view()),

    #Habitacion
    path('habitacion/', views.HabitacionCreateView.as_view()),
    path('habitacionlist/', views.HabitacionListView.as_view()),
    path('habitacion/<int:pk>/', views.HabitacionDetailView.as_view()),
    path('habitacion/update/<int:pk>/', views.HabitacionUpdateView.as_view()),
    path('habitacion/delete/<int:pk>/', views.HabitacionDeleteView.as_view()),

    #Servicio
    path('servicio/', views.ServicioCreateView.as_view()),
    path('serviciolist/', views.ServicioListView.as_view()),
    path('servicio/<int:pk>/', views.ServicioDetailView.as_view()),
    path('servicio/update/<int:pk>/', views.ServicioUpdateView.as_view()),
    path('servicio/delete/<int:pk>/', views.ServicioDeleteView.as_view()),

    #Reserva
    path('reserva/', views.ReservaCreateView.as_view()),
    path('reservalist/', views.ReservaListView.as_view()),
    path('reserva/<int:pk>/', views.ReservaDetailView.as_view()),
    path('reserva/update/<int:pk>/', views.ReservaUpdateView.as_view()),
    path('reserva/delete/<int:pk>/', views.ReservaDeleteView.as_view()),

    #factura
    path('factura/', views.FacturaCreateView.as_view()),
    path('facturalist/', views.FacturaListView.as_view()),
    path('factura/<int:pk>/', views.FacturaDetailView.as_view()),
    path('factura/update/<int:pk>/', views.FacturaUpdateView.as_view()),
    path('factura/delete/<int:pk>/', views.FacturaDeleteView.as_view()),
]
